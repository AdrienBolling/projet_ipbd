import pandas
entr = pandas.read_csv("bdd/Varmod_crent_2021.csv", sep = ';')
entr['COD_VAR'].unique()
entr_artisan = entr.loc[entr['COD_VAR'] == 'ARTISAN']
entr_artisan.rename(columns = {'COD_MOD' : 'ARTISAN'}, inplace = True)
artisan = entr_artisan.LIB_MOD.str[4:]
entr_artisan['artisan'] = artisan
entr_CJ1 = entr.loc[entr['COD_VAR'] == 'CJ1']
entr_CJ1.rename(columns = {'COD_MOD' : 'CJ1', 'LIB_MOD' : 'Catégorie entreprise 1 '}, inplace = True)
entr_CJ2 = entr.loc[entr['COD_VAR'] == 'CJ2']
entr_CJ2.rename(columns = {'COD_MOD' : 'CJ2'}, inplace = True)
entr_CJ2[['CJ2']] = entr_CJ2[['CJ2']].astype('int64')
categorie = entr_CJ2.LIB_MOD .str[4:]
entr_CJ2['categorie'] = categorie
entr_taille = entr.loc[entr['COD_VAR'] == 'TAILLE']
entr_taille.rename(columns = {'COD_MOD' : 'TAILLE'}, inplace = True)
et = entr_taille.LIB_MOD .str[4:] 
entr_taille['nb_salarié'] = et
entr_taille[['TAILLE']] = entr_taille[['TAILLE']].astype('int64')
entr_A10 = entr.loc[entr['COD_VAR'] == 'A10']
entr_A10.rename(columns = {'COD_MOD' : 'A10', 'LIB_MOD' : 'Activite nomenclature 9'}, inplace = True)
entr_A21 = entr.loc[entr['COD_VAR'] == 'A21']
entr_A21.rename(columns = {'COD_MOD' : 'A21', 'LIB_MOD' : 'Activite nomenclature 21'}, inplace = True)
entr_A38 = entr.loc[entr['COD_VAR'] == 'A38']
entr_A38.rename(columns = {'COD_MOD' : 'A38', 'LIB_MOD' : 'Activite nomenclature 38'}, inplace = True)
entr_A64 = entr.loc[entr['COD_VAR'] == 'A64']
entr_A64.rename(columns = {'COD_MOD' : 'A64', 'LIB_MOD' : 'Activite nomenclature 64'}, inplace = True)
entr_APE = entr.loc[entr['COD_VAR'] == 'APE']
entr_APE.rename(columns = {'COD_MOD' : 'APE'}, inplace = True)
activite = entr_APE.LIB_MOD.str[7:]
entr_APE['activite'] = activite
entr_COM = entr.loc[entr['COD_VAR'] == 'COM']
entr_COM.rename(columns = {'COD_MOD' : 'COM', 'LIB_MOD' : 'commune implentation'}, inplace = True)
entr_ZE = entr.loc[entr['COD_VAR'] == 'ZE']
entr_ZE.rename(columns = {'COD_MOD' : 'ZE', 'LIB_MOD' : 'Ville'}, inplace = True)
entr_ZE[['ZE']] = entr_ZE[['ZE']].astype('int64')
entr_UU = entr.loc[entr['COD_VAR'] == 'UU']
entr_UU.rename(columns = {'COD_MOD' : 'UU', 'LIB_MOD' : 'Unite urbaine'}, inplace = True)
entr_AAV = entr.loc[entr['COD_VAR'] == 'AAV']
entr_AAV.rename(columns = {'COD_MOD' : 'AAV', 'LIB_MOD' : 'Attraction des villes'}, inplace = True)
entr_dep = entr.loc[entr['COD_VAR'] == 'DEP']
entr_dep.rename(columns = {'COD_MOD' : 'DEP'}, inplace = True)
departement = entr_dep.LIB_MOD.str[5:]
entr_dep['departement'] = departement
entr_reg = entr.loc[entr['COD_VAR'] == 'REG']
entr_reg.rename(columns = {'COD_MOD' : 'REG'}, inplace = True)
entr_reg[['REG']] = entr_reg[['REG']].astype('int64')
pays = entr_reg.LIB_MOD.str[5:]
entr_reg['pays'] = pays
entr = pandas.read_csv("bdd/crent_2021.csv", sep = ';')
entr['DEP']= entr['DEP'].astype(str)
Entreprise = entr.merge(entr_reg[['REG', 'pays']], on = 'REG').merge(entr_dep[['DEP', 'departement']], on = 'DEP').merge(entr_ZE[['ZE', 'Ville']], on = 'ZE').merge(entr_APE[['APE', 'activite']], on = 'APE').merge(entr_A64[['A64', 'Activite nomenclature 64']], on = 'A64').merge(entr_A38[['A38', 'Activite nomenclature 38']], on = 'A38').merge(entr_A21[['A21', 'Activite nomenclature 21']], on = 'A21').merge(entr_A10[['A10', 'Activite nomenclature 9']], on = 'A10').merge(entr_CJ1[['CJ1', 'Catégorie entreprise 1 ']], on = 'CJ1').merge(entr_CJ2[['CJ2', 'categorie']], on = 'CJ2').merge(entr_artisan[['ARTISAN', 'artisan']], on = 'ARTISAN').merge(entr_taille[['TAILLE', 'nb_salarié']], on = 'TAILLE')

Entreprise[['pays', 'departement', 'Ville', 'activite', 'Activite nomenclature 38', 'Activite nomenclature 64', 'Activite nomenclature 21', 'Activite nomenclature 9', 'Catégorie entreprise 1 ', 'categorie', 'artisan', 'nb_salarié']]
Entreprise.to_csv('bdd/donnees.csv', index = False, header=True)
