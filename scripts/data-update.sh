#!/bin/bash
#
#
rm ../data/bdd/*.zip

curl -o ../data/bdd/etab.zip https://www.insee.fr/fr/statistiques/fichier/6046108/sidemo2021_crets_2021_csv.zip
curl -o ../data/bdd/entr.zip https://www.insee.fr/fr/statistiques/fichier/6046108/sidemo2021_crent_2021_csv.zip

echo "Téléchargement des données terminé !\n"

unzip -q -o ../data/bdd/entr.zip -d ../data/bdd/
unzip -q -o ../data/bdd/etab.zip -d ../data/bdd/

echo "Désarchivage des données terminé !\n"

python3 ../data/data-pre-treatement.py

echo "Traitement des données terminé !\n"
