#!/bin/bash
#
#

docker-compose -f ./docker-compose.yml down -v
docker volume list | grep elastic | awk '{ print $2 }' | xargs docker volume rm --force
