#!/bin/bash
#
#

docker network create elastic 2>/dev/null
docker-compose -f ./docker-compose.yml up -d 

my_ip=`ip route get 1 | awk '{ for (i=1;i<=NF;i++) { if ( $i == "src" ) { print $(i+1) ; exit } } }'`
echo "\n"
echo "Elasticsearch : http://${my_ip}:9200\n"
echo "Kibana : http://${my_ip}:5601"
